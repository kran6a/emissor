import emissor from "./app.mjs";
const em = emissor({compose: true, async: true});

const delay = ms => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const startTime = Date.now();

em.on("test", async a=>{
    const endTime = Date.now();
    console.error("first fn", endTime-startTime, "ms");
    return a*20;
});

em.on("test", async a=>{
    await delay(a);
    const endTime = Date.now();
    console.error("second fn", endTime-startTime, "ms");
});

em.emit("test", 1000); //"first fn" handler fires inmediately since there is no await inside the function body "second fn" handler fires after 1000*20 ms = 20.000ms
em.emit("test", 2000); //"first fn" handler fires inmediately since there is no await inside the function body "second fn" handler fires after 2000*20 ms = 40.000ms
em.emit("test", 2000); //Same as above, the total program runtime is 40.000ms + deviation