export default (opts = {compose: true, async: true})=>{
    const dispatchTable = new Map();
    const compose = opts.async ?
        (...fns) => fns.reduce((f, g) => async (...args) => g(await f(...args))) :
        (...fns) => fns.reduce((f, g) => (...args) => g(f(...args)));
    if (opts.compose) {
        if (opts.async) {
            return {
                emit(event, ...payload) {
                    if (dispatchTable.has(event))
                        dispatchTable.get(event)(...payload);
                },
                on(event, fn) {
                    dispatchTable.has(event) ? dispatchTable.set(event, compose(dispatchTable.get(event), fn)) : dispatchTable.set(event, fn);
                }
            };
        }
        else{
            return {
                emit(event, ...payload) {
                    if (dispatchTable.has(event))
                        dispatchTable.get(event)(...payload);
                },
                on(event, fn) {
                    dispatchTable.has(event) ? dispatchTable.set(event, compose(dispatchTable.get(event), fn)) : dispatchTable.set(event, fn);
                }
            };
        }
    }
    //!compose -> the dispatch table consists on string - array<fn> pairs
    else{
        return {
            emit(event, ...payload) {
                if (dispatchTable.has(event))
                    dispatchTable.get(event).forEach(fn=>fn(...payload));
            },
            on(event, fn) {
                dispatchTable.has(event) ? dispatchTable.set(event, [...dispatchTable.get(event), fn]) : dispatchTable.set(event, [fn]);
            }
        };
    }
};